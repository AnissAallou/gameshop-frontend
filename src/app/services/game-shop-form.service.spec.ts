import { TestBed } from '@angular/core/testing';

import { GameShopFormService } from './game-shop-form.service';

describe('GameShopFormService', () => {
  let service: GameShopFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GameShopFormService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
